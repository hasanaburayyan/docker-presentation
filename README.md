<h1>Welcome To Docker</h1>
<h3>By: Hasan Abu-Rayyan</h3>


#### Purpose of this repo:
To give a deep dive into Docker

### AGENDA
- [What is Docker?](what-is-docker/README.md) 
    - [Docker Images](what-is-docker/docker-images/README.MD)
    - [Dockerfiles](what-is-docker/docker-files/README.md)
    - [Multistaged Builds](what-is-docker/multi-staged-builds/README.md)
- [Advanced Topics](advanced-topics/README.md)
    - [The Docker Daemon](advanced-topics/the-docker-daemon/README.md)
    - [Docker in Docker](advanced-topics/docker-in-a-docker/README.md)
    - [Docker Networking](advanced-topics/docker-networking/README.md)
- [Container Orchestration](container-orchestration/README.md)
    - [Docker Compose](container-orchestration/docker-compose/README.md)
    - [Docker Swarm](container-orchestration/docker-swarm/README.md)
    - [Kubernetes](container-orchestration/kubernetes/README.md)
    
### Coming Soon ...

- Security Risks
- Privileged Users
- Docker Image Registries
- Container segmentation (namespaces)



##### Work In Progress...
Please note this repo is a work in progress therefore some presentation topics and hands on demos may not be complete.
