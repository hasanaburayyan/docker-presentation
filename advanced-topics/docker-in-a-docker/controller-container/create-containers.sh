#!/usr/bin/env sh

# Create 5 containers one every 5 seconds
for number in `seq 0 4`
  do
    docker run -d --name dind_container_$number nginx
    sleep 5
  done

# Kill stop and remove containers 1 every 3 seconds
for id in $(docker ps -aq)
  do
    docker stop "${id}"
    docker rm "${id}"
    sleep 3
  done
