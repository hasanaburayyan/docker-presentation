## Docker In A Docker

Sometimes we want to control docker orchestration within a controlled environment, like a container. This means we need to be able 
to spin up docker containers from within a docker container. 

### Forget The Explanation Lets Just Do It!
We will use the official docker image for Docker in Docker conviently named `docker`

Lets create a container and use the `-it` flag to interactively jump into the container

`docker run -it docker` 

### So you are back...
So what were we missing? Pretty simple actually. We already learned a bit about the Docker daemon. We know that the daemon listens to Docker API calls. Well
how does the daemon listen for those API calls? It uses a UNIX socket called the docker.sock If we want our container to be able to communicate to the docker
daemon then we need to give the container access to the socket

`docker run -it -v /var/run/docker.sock:/var/run/docker.sock docker`

### Dont let the name fool you
The term Docker in Docker (dind) might lead you to believe that we are creating a container and then using that container and creating containers inside of that
container. The reference to Docker in Docker means that we have access to the Docker daemon from withing
a docker container. So really the Controller container is creating sibling containers and not so much children containers.

### FALSE STRUCTURE:
- Host Machine
    - Controller Container
        - dind container
        - dind container
        - dind container
### ACTUAL STRUCTURE:
- Host Machine
    - Controller Container
    - dind container
    - dind container
    - dind container


## Controller Demo!
Build Our Controller [See Dockerfile](controller-container/Dockerfile)

`docker build -t my-controller:latest  ./controller-container/.`

Run The Controller And Watch The Magic

`docker run -d --rm --name my-controller -v /var/run/docker.sock:/var/run/docker.sock my-controller && watch docker ps`

In case of Bad Demo: Cleanup

`./clear_running_containers.sh`

[Next Topic](../docker-networking/README.md)

[To Advanced Topics](../README.md)

[To Agenda](../README.md)
