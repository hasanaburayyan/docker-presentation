# Advanced Topic Section

## Overview
In this section we will go over some of the more advanced concepts in Docker. While containers solve many common problems across a wide range of computer science topics. 
There are some hurdles that come with them as well. Learning to master those will be essential if you want to build Highly Available, Elastic, Resilient, and Secure applications.

### Topics:
- [The Docker Daemon](the-docker-daemon/README.md)
- [Docker In Docker](docker-in-a-docker/README.md)
- [Docker Networking](docker-networking/README.md)

[To Agenda](../README.md)
