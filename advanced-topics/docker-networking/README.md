# Docker Networking
One of the most powerful features of using Docker containers is the network capabilities. Containers can be connected or segmented from each other fairly easily utilizing docker networks.

### Network Drivers
Docker's networking subsystem uses drivers. The default network drivers are:
- bridge
    - This is the default network driver. If no driver is specified bridge will be selected. Bridge networks are usually best when your application runs in standalone containers that need to communicate with each other
    - Should be noted that unlike user defined bridge networks the default bridge does not use automatic DNS resolution. Therefore IP addresses must be used for communication
- host
    - Removes network isolation between container and docker host.
    - Best for stand alone containers that need to access hosts networking directly
- overlay
    - This driver connects multiple docker daemons together.
    - Which also enables swarm services to communicate with each other.
- macvlan
    - Allows you to assign MAC address to a container, which will make it appear as a physical device on your network
- none
    - Disables ALL networking. 

### Lets Create Our Own bridge network!
First check your current docker network `docker network ls`

Now lets make our own `docker network create my-network -d bridge` (we dont need the -d bridge since it is the default option this is just for visibility)

Boom that is it we did it our own Docker network!

### Putting Network To Use!
Lets create a cool little express server [See Express Server Code](express-server/app.js)

First build our custom express image `docker build -t my-express-server:latest ./express-server/.`

Next stand that bad boy up! `docker run -d --name express-app my-express-server`

Just to make sure our express app is running lets check the containers logs `docker logs express-app` We should see something like this:
![Running Express App](resources/running-express-app.png)

Hop into you own container and try to curl `docker run --rm -it curlimages/curl sh`

Take a second here to think about how we should we try and hit our web app.
- What is the context of it's localhost:3000
- What is exposed?
- What is the container's network?

Trial by Fire!
curl your local host `curl localhost:3000`

Okay so maybe we try to curl the container by name? `curl express-app:3000`

Still no dice.. In another terminal Lets take a look at our container with `docker inspect express-app`

Ahh there we go `curl <container_ip_address>:3000` feel free to type `exit` to leave container when done

![container ip address](resources/ip-address.png)

#### Wait Where Was Our Network?!
You may have noticed by now we never attatched our network to our container. Luckily for us even though you can specify networks at creation time, 
we can also attatch networks to running containers. Lets go ahead and do so. `docker network connect my-network express-app` after connecting lets inspect again `docker inspect express-app`

![My Network Attatched](resources/my-network.png)

#### Remember User Defined Bridge Networks Have Automatic DNS!!
With our custom bridge networks we get automatic DNS so lets hop back in our curl image but this time atatch it to the same custom network `docker run --rm -it --network my-network curlimages/curl sh`

Now curl with DNS (using the containers name) `curl express-app:3000`

<!-- docker run --rm -p 80:80 -p 3000:3000 -d --name webapp my-webapp -->

[To Advanced Topics](../README.md)

[To Agenda](../../README.md)
