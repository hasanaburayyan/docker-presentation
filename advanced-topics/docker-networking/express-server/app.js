const express = require('express')
const bodyParser = require('body-parser');
const app = express()
const port = 3000

let pingcount = 0;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.get('/health', (req, res) => {
    response = {
        message: "Healthy And Ready To Work!",
        pingCount: ++pingcount
    }
    res.send(response).status(200)
})

app.listen(port, () => {
    console.log(`My I love learning docker app listening at http://localhost:${port}`)
})
