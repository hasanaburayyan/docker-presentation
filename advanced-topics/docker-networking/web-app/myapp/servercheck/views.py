from django.shortcuts import render
from django.http import HttpResponse
import requests
import json


# Create your views here.
def index(request):
    return HttpResponse("Hello Server Check!")

def message(request):
    return "SHHH it is a secret"

def check(request):
    """View function for home page of site."""

#     # Generate counts of some of the main objects
#     num_books = Book.objects.all().count()
#     num_instances = BookInstance.objects.all().count()
#
#     # Available books (status = 'a')
#     num_instances_available = BookInstance.objects.filter(status__exact='a').count()
#
#     # The 'all()' is implied by default.
#     num_authors = Author.objects.count()
#
#     context = {
#         'num_books': num_books,
#         'num_instances': num_instances,
#         'num_instances_available': num_instances_available,
#         'num_authors': num_authors,
#     }

    print('Context:')
    context = {
        'title': 'Server Checks',
        'server_1': checkHost('express-server-network-1'),
        'server_2': checkHost('express-server-network-2'),
    }
    print(context)
    # Render the HTML template index.html with the data in the context variable
    return render(request, 'index.html', context=context)


def checkHost(host_name):
    try:
        r = requests.get(f"http://{host_name}:3000/health")
    except:
        context = serverDown(f"{host_name} Check", 503)
        return context

    if r.status_code != 200:
        return serverDown(host_name, r.status_code)

    body = json.loads(r.content)
    context = {
        'title': f"{host_name} Check",
        'message': body.get('message'),
        'ping_count': body.get('pingCount'),
        'status': r.status_code
    }
    return context

def serverDown(title, status_code):
    context = {
        'title': title,
        'message': 'Server Unavailable',
        'ping_count': 'Server Unavailable',
        'status': status_code
    }
    return context
