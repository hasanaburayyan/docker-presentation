from django.urls import path

from . import views

urlpatterns = [
#     path('health', views.check, name='check'),
    path('message', views.message, name='message'),
    path('', views.check, name='index'),
]
