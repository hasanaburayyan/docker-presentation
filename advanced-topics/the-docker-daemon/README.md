# The Docker Daemon
The Docker daemon is a service that runs on the host operating system.

## What is a service?
A service is an application(s) that runs in the background and waits to be used. Commonly we configure services with `systemctl` to ensure they start when your machine boots 
A few examples of popular linux services include:
- crond (Runs programs/commands on a schedule)
- sendmail (Mail Server Daemon)
- httpd (Apache Web Server Daemon used to provide dynamic HTML content)
- dockerd (The Docker Daemon)

Keep in mind that the term Daemon and Services are pretty synonymous. Both are background services but Daemon is a term Linux/Unix Culture, Windows refers
to daemons as services.

To view running services on your local machine you can `systemctl --type=service --state=active`

## Why Is The Docker Daemon Important?
The Docker daemon is crucial to the Docker Engine. It listens for Docker API requests and manages Docker objects such as images, containers, networks and volumes.

So let's make sure our Docker daemon is running `systemctl status docker`

#### The Docker Engine
- Server (runs the docker daemon)
- REST API
- CLI

![The Docker Engine](resources/engine-components-flow.png)


[Next Topic](../docker-in-a-docker/README.md)

[To Advanced Topics](../README.md)

[To Agenda](../../README.md)
