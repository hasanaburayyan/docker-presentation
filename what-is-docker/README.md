## What Is Docker?

- A tool that uses OS-level virtualization to deliver software packages called containers
- Comes in a free tier and a enterprise paid version. (Main difference being trusted images registries and Docker support)

### How is a container different than a VM?
<table>
    <thead>
        <td>Category</td>
        <td>Containers</td>
        <td>Virtual Machine</td>
    </thead>
    <tr>
        <td>Size</td>
        <td>Measured in megabytes</td>
        <td>Measure ing gigabytes</td>
    </tr>
    <tr>
        <td>Use case</td>
        <td>Packaged application that can be easily moved and duplicated</td>
        <td>Semi permanent instance that will allocated IT resources</td>
    </tr>
    <tr>
        <td>Implementation</td>
        <td>Virtualize host OS to run multiple workloads on the Host OS</td>
        <td>Virtualize Hardware to run multiple OS</td>
    </tr>
</table>

![alt text](https://blog.netapp.com/wp-content/uploads/2016/03/Screen-Shot-2018-03-20-at-9.24.09-AM-935x500.png)

when we think VM we consider the VCPU, RAM, Volume etc. 

[To Next Topic](docker-images/README.MD)

[To Agenda](../README.md)
