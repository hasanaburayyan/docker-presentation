## Multi Staged Building
- Save on image size!
- More secure

Process involves creating a helper image to prep things for final image.

### The Analogy You Never Asked For
Imagine we decide to build a chair in the garage. We are going to the store and grab all the supplies
- Wood
- Screws
- Table Saw
- Drill
- Miter Saw
- Wood Glue
- More wood
- Wait where did the screws go?

Then during that time we are going back and forth for supplies we are also making a mess with our constant sanding, drilling, sawing, breaking things.

Then 2 weeks later we are done, we have a beautifully hand crafted chair. Time to show it off!

Now do we:

A. Bring people into our disaster zone garage and ask them to look past the mess and admire the chair?

OR

B. Take our wonderful new chair. Stage it on the nice clean front porch, and never open the garage again?

Obviously B and the same applies for our Docker Images. We want our images to contain all the pretty bits and none of the mess that it took to make them
nice and appealing.


### Why do this?

Several Advantages
- Reduce Attack Surface 
    - Smaller images generally mean less libraries
- Performance
    - Smaller images are easier and faster to move
    - Improves performance of deployment process.
- Maintainability
    - Reducing the amount of libraries to manage makes upgrading easier.

[To Agenda](../../README.md)
