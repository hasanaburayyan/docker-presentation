## Docker Files
Docker files are text files that contain Docker Directives which can be used by the docker
cli in order to create docker images. 

Docker Directives include:
 - FROM
    - Initialize build stage and set base image
 - RUN
    - Execute commands in new layer on top of current image, then commits results
 - COPY
    - Copies files or directories from \<src\> then adds them to \<dest\> in container file system 
 - ADD
    - All functionality of COPY with additional capabilites of (multiples sources, url based sources, etc)
 - MAINTAINER (deprecated by label)
    - Sets Author field in Image metadata. Has been deprecated by LABEL
 - LABEL
    - Add Metadata information to image in KEY VALUE pairs
 - ENV
    - Set environment variables for image using KEY VALUE pairs (PASSWORD=secret)
 - CMD
    - Sets default command for the container to use when running an image. 
 - ENTRYPOINT
    - Configure container to be ran as executable. Similar to COMMAND but is not overridden by providing command at run tim.
 - HEALTH CHECK
    - Define how docker can determine if container is healthy
 
To create a docker image using a dockerfile we need to utilize the docker cli and run the build command,
`docker build .` In this command we are going to use `.` to let docker search through the current directory and find 
a dockerfile to use.

[To Next Topic](../multi-staged-builds/README.md)

[To Agenda](../../README.md)
