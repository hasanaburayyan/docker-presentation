# Docker Swarm

Docker swarm is an orchestration tool for docker containers that can be deployed across multiple hosts. One of the coolest parts about swarm is 
it comes native with your docker install!

Key Features of Swarm:
- Declarative service model
    - Allows us to define our services declarative manner similar to docker-compose
- Scaling
    - We can utilize our swarm manager an easily scale up or down our service
- State reconciliation
    - Our manager node will attempt to keep our cluster in a desired state.
    - If we want 10 replicas and 3 go down, our manager will recreate 3
- Mutli-host networking
    - Our swarm cluster can use an overlay network, and the manager will automatically handle assigning addresses
- Load Balancing
    - Internal to our swarm we can specify how to distribute service containers between nodes
- Service Discovery
    - Swarm manager handles assigning each service in the swarm a unique DNS name. Then load balances running containers.

And More...



## Hands On Examples

Awesome Docker Swarm Kata:

(Credit goes out to Cody Wimer)

https://confluence.cas.org/display/OPS/Docker+Swarm+Kata



[Next Topic](../kubernetes/README.md)

[To Advanced Topics](../README.md)

[To Agenda](../../README.md)
