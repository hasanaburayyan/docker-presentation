# Container Orchestration

With a solid understanding of what docker is and what are some of the more tedious of difficult subjects to manage, we can start to gain a much appreciated understanding of
container orchestration. Container Orchestration can use definitions in config files to automate: 
- deployment
- scaling
- networking
- scheduling
- load balancing
- monitoring
- resource sharing
- And More!

With the additional capabilities provided by orchestration tooling we can design highly available deployments that are elastic and fault tolerant. The big buy being all components are containerized

### Topics:

- [Docker Compose](docker-compose/README.md)
- [Docker Swarm](docker-swarm/README.md)
- [Kubernetes](kubernetes/README.md)

[To Agenda](../README.md)
