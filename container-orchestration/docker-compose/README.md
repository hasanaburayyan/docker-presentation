## What is docker-compose
Compose is a tool that was created for running multi-container applications. It utilizes a YAML file that will define
application services. Allowing users to stand all required services for an application with just one command.

### Not docker cli native
When you install Docker you will not get docker-compose by default. The docker-compose cli is separate. 

### Where Does Compose Excel?
Docker compose is great for local development environments. If your developers require multiple services to run and need to handle communication between those services
docker compse could be exactly what you would want to use.

#### Example Scenario
Developers are working on a react web application that needs to store and retrieve information from a mongo. The retrieval is done via react app that sends requests
to an express server. The express server will handle the actual retrieval. 

Identified Services:
- React Web Application
- Express Server
- Mongodb

Using Docker compose we can use the docker images we create and use during development and just stand them up locally, orchestrating network communication and environment
variable injection. We can also cluster services and create 3 mongodb instances (if we so choose). Our devs can start this entire environment with just `docker-compose up -d`

[Example Compose FIle (display purpose only)](docker-compose.yml)

### Why Compose Alone Is Not An Ideal Production Solution
The main reason is that docker-compose runs on a single host and cannot deploy containers across multiple hosts using remote Docker daemons.

Ideally we want to reduce our single point of failures and ensure High Availability meaning production deployments should have anti-affinity (not run clustered services on a single host)

[Next Topic](../docker-swarm/README.md)

[To Advanced Topics](../README.md)

[To Agenda](../../README.md)
