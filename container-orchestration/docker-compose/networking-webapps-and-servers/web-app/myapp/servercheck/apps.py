from django.apps import AppConfig


class ServercheckConfig(AppConfig):
    name = 'servercheck'
