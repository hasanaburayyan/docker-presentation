## Docker Compose Network Demo

#### Goal:
Create a web application that listens on two custom bridge networks. Then create 2 express servers, one on each network. Our web application
will need to prove it's ability to communicate with both servers. Our application should resemble: 

![Goal Application](resources/full-connectivity.png) 

#### Start Here:
To Build the required images use the [docker-compose.yml](docker-compose.yml) found in this directory. Run `docker-compose build`

This will result in 2 different images being created:
- my-webapp
    - This image will be used to launch our web application. 
    - See more details in the [Dockerfile](web-app/Dockerfile)
- my-express-server
    - This image will be used to create express servers
    - See more details in the [Dockerfile](express-server/Dockerfile)
    
Take a moment to look through the docker-compose.yml after taking a look go ahead and orchestrate our deployment with `docker-compose up -d`

Once the application is stood up just visit <a href="http://localhost:80/health">http://localhost:80/health </a>


#### Issue
Something is wrong, server 2 is not available. Take a look and see if you can find any issues in the docker-compose.yml Dont worry if you cant find
the answer is below!

![Bad Server Image](resources/Bad%20Server%20Checks.png)


### Solution:
Upon further examination of the docker-compose.yml we can see that express-server-2 is missing a networks block thus was never brought up on my-network-2
to correct this properly lets add the network to its yaml. Your express-server-2 block should look like this:
  
```$xslt
express-server-2:
image: my-express-server
container_name: express-server-network-2
networks:
  - my-network-2
```  

After you fix this, just re-run `docker-compose up -d` It will detect the changes to the yaml and update our running servers. Now you should see both health
checks working!
