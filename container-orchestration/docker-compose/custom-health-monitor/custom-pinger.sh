#!/usr/bin/env bash

declare ADDRESS=$1

function pingAddress() {
    while [ True ]; do
        curl -s -I "${ADDRESS}"
        sleep 3
    done
}

function main {
  echo "${ADDRESS}"
  pingAddress
}

[[ "${0}" == "${BASH_SOURCE[0]}" ]] && main "${@}"
