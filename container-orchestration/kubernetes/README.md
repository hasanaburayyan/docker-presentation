# Kubernetes

An open-source system for automating deployment, scaling, and management of containerized applications!

Used by Google for over 15 years

Backed by Cloud Native Computing Foundation (CNCF)

Organization made easy with pods!

Recommended for medium to large clusters that are running complex applications

Comes with a learning curve (requiring understanding of kubectl CLI ontop of docker)

Incompatible with existing docker cli and compose tools

Hefty install and setup, Recommend using tools for the setup (Rancher, AWS EKS, etc)

## Hands On Examples

Awesome Kubernetes Kata:

(Credit goes out to Cody Wimer)

https://confluence.cas.org/display/~jqw43/Kubernetes+Kata

[To Advanced Topics](../README.md)

[To Agenda](../../README.md)
